name             'lsd-local'
maintainer       'Turner Broadcasting, Inc.'
maintainer_email 'jerry.you@web.turner.com'
license          'All rights reserved'
description      'Installs/Configures lsd-local'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends		 "lsd-riak"
