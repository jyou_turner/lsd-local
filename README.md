lsd-local Cookbook
==================
This cookbook helps to create a LSD sandbox by wrapping the lsd-riak recipes and customize it to local scope.

LSD is designed to serve frequently changed data to large number of users. For example, structure data group serves billions of scorting data files every year to LSD for various properties including NBA, NCAA, SI, PGA, Bleacher Report etc. To create a regular LSD environment, you'd normally need:
1, create a data bag and add configurations for the specific property, ie, https://chef.vgtf.net/databags/lsd/items/nba. One of the attributes of the data bag is the host name of the LSD server, for example, dataint.nba.com or data.nba.com
2, add the lsd-riak, lsd-web recipes to the run list, which will install Nginx server and create virtual host to serve the specified host ie, dataint.nba.com
3, the LSD cookbook uses Riak as backend, and cluster the Riak servers based on the scope.
4, the LSD cookbook also install Haproxy at front of the cluster.

LSD is really not designed to be a sandbox, since it is not meant to "scope down". However, within our team (Structured Data), we constantly need such a environment, for example, one engineer is running simulation on NBA regular season, and another engineer is testing NBA play-off changes, and we only have couple of LSD environemtns (dataref.nba.com, dataint.nba.com) available. We need a way to create "sandbox" which has every thing (torn, activemq, receiver, publisher, LMS, LSD) in one box so we can run our testing without stepping on each other.

This cookbook has everything necessary to create a LSD sandbox through utilizing LSD-Riak cookbook with additional configurations:
1, a data bag "lsd-sandbox" exists to hold LSD configuations for the properties. Check https://chef.vgtf.net/databags/lsd-sandbox. It already has nba and ncaa settings, which are quite simple. Note that the host name is like datasandbox.nba.com and datasandbox.ncaa.com. Those hosts don't exist in DNS. They are only used as virtual host in Nginx. 
2, Remember LSD-Riak cluster the riak servers by scope. We need to disable this feature by setting the scope to the IP address of the VM.
node.override['scope'] =  node[:fqdn]
3, Then we create the LSD service with the lsd-riak::riak and lsd-riak::web recipes             
include_recipe 'lsd-riak::riak'
include_recipe 'lsd-riak::web'
4, To publish to the local LSD, you can post the file by
curl --binary-data @"test.xml" http://localhost/nba/xml/noseason/test.xml
In our work, we use LMS to publish to LSD. To work with the sandbox, all you need to do is to change the lms to post to xyz.cnn.vgtf.net. Or, you can set up a LMS at local, and publish to localhost.

5, How to read the file from LSD? you can not use the "datasandbox.nba.com", the only thing available is the VM http://xyz.cnn.vgtf.net. What we do in this cookbook is to add rule to the Haproxy to forward particular port to the virtual host


frontend lsd_nba_front
       bind *:20080
       mode http
       reqirep ^Host:.*  Host:\ datasandbox.nba.com
       default_backend lsd_sandbox_backend

backend lsd_sandbox_backend
       mode http
       server server1 127.0.0.1:80
  

so, if a NBA feed normally exists at http://data.nba.com/data/5s/xml/partners/noseason/league/lpbbCurrent.xml, then in the sandbox, you will use
http://xyz.cnn.vgtf.net:20080/data/5s/xml/partners/noseason/league/lpbbCurrent.xml



Requirements
------------
lsd-sandbox data bag
lsd-riak cookbook

Attributes
----------
* node[`lsd-local`][`port`] - the port number of LSD in the sandbox, by default it is 20080
* node[`lsd-local`][`virtual_host`] - the virtual host of the LSD, which is also defined in the data bag, ie, datasandbox.nba.com
Above two attribute help to configure the Haproxy to map the specified port number to the virtual host 

Usage
-----
#### lsd-local::lsd-riak-local

If the propety already exists in the data bag, all you need to do is to include `lsd-local::lsd-local-riak` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[lsd-local::lsd-riak-local]"
  ]
}
```

To add a new property, just update the dat bag "lsd-sandbox" 

Contributing
------------

License and Authors
-------------------
Authors: jerry.you@turner.com
