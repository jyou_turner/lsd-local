#
# Cookbook Name:: lsd-local
# Recipe:: lsd-riak-local
#
# Copyright 2013, Turner Broadcasting, Inc.
#
# All rights reserved - Do Not Redistribute
#
node.default['lsd-riak']['data_bag'] = 'lsd-sandbox'
node.override['scope'] =  node[:fqdn]

Chef::Log.info("create the LSD riak service with lsd-riak cookbook.
Chef::Log.info("note - sometime the below recipe fail at restarting riak cluster. And if you see that when you run chef-client, you can manually start riak by sudo service riak, then, run the chef-client again."

include_recipe 'lsd-riak::riak'
include_recipe 'lsd-riak::web'
#
# lsd-riak create haproxy.cfg with template, we need to append our own port forwarding, ie, 20080 to datasandbox.nba.com
# and 20081 to datasandbox.ncaa.com etc to the haproxy.cfg file.
# This is not the best way to manage the haproxy.cfg, could use the partial template once chef 11 is available

template "/tmp/lsd_local_haproxy.cfg" do
  owner "root"
  group "root"
  mode "0444"
  variables(
            :lsd_port      => node['lsd-local']['port'],
            :lsd_virtual_host => node['lsd-local']['virtual_host']
            )
end

#
# append to haproxy.cfg if marker is not found
#
bash "append_to_haproxy_cfg" do
    user "root"
    code <<-EOF
        cat /tmp/lsd_local_to_haproxy.cfg >> /etc/haproxy/haproxy.cfg
        rm /tmp/lsd_local_to_haproxy.cfg
    EOF
    not_if "grep -q lsd_local_to_proxy /etc/haproxy/haproxy.cfg"
    notifies :restart, "service[haproxy]"
end

